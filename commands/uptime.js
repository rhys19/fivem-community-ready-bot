const Discord = require("discord.js")

module.exports.run = async (bot, message, args) => {
  const botcmds = message.settings.botcmds

  if (message.channel.id === `${message.settings.botcmds}`) {

  message.delete();
    let totalSeconds = (bot.uptime / 1000);
    let hours = Math.floor(totalSeconds / 3600);
    totalSeconds %= 3600;
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;

    // let uptime = `${hours} hours, ${minutes} minutes`;

    // return message.channel.send(uptime);

    let uptimeEmbed = new Discord.RichEmbed()
    .setDescription("Server Manager Bot Uptime")
    .setColor("#fef836")
    .addField("Hours", hours)
    .addField("Minutes", minutes)
	.addField("Seconds", seconds)
    .setTimestamp()
.setFooter(`${bot.config.footer}`) 
    message.channel.send(uptimeEmbed).then(msg => msg.delete(10000));
  } else {
    message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds).then(msg => msg.delete(5000))
  }
  }



exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 1
};

exports.help = {
  name: "uptime",
  category: "System",
  description: "Shows Bot Uptime",
  usage: "uptime"
};