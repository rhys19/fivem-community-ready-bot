exports.run = (client, message, args) => {
  message.delete()
client.user.setStatus('invisible') // Online, Idle, DND, Invisible
let embed1 = new Discord.RichEmbed()
.setColor([255, 0, 0])
.setThumbnail(`${message.guild.iconURL}`)
.addField("Nuclear Bot Status", "Set to Offline!")
.setFooter(`${client.config.footer}`) 
message.channel.send({embed: embed1})
  message.delete()
};

const Discord = require('discord.js');
//const fs = require("fs");
//const db = require("quick.db");
//onst i = require("fs");
const client = new Discord.Client(); // This uses the discord.js package to setup a client

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "BotOwner"
};


exports.help = {
  name: "offline",
  category: "Miscelaneous",
  description: "Set bot status to offline!",
  usage: "offline"
};
