exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
  const botcmds = message.settings.botcmds
  message.delete()

  if (message.channel.id === '629107222802202634') {
  const msg = await message.channel.send("Ping?");
  msg.edit(`Pong! Latency is ${msg.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
  message.delete()
} else {
  message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds).then(msg => msg.delete(5000))}
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "ping",
  category: "Miscelaneous",
  description: "It... like... pings. Then Pongs. And it\"s not Ping Pong.",
  usage: "ping"
};
