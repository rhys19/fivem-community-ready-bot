exports.run = (client, message, args, level) => {
  const botcmds = message.settings.botcmds
  const ticketcmds = message.settings.ticketcmds

  //if (message.channel.id === ticketcmds, botcmds) {
    message.delete()
const Discord = require('discord.js');
  const emoji = client.emojis.find(emoji => emoji.id === "628941324523012096")
  const reason = message.content.split(" ").slice(1).join(" ");
  if (!message.guild.roles.exists("name", "Support Team")) return message.channel.send(`This server doesn't have a \`Support Team\` role made, so the ticket won't be opened.\nIf you are an administrator, make one with that name exactly and give it to users that should be able to see tickets.`);
  if (message.guild.channels.exists("name", "ticket-" + `${message.author.tag}`)) return message.channel.send(`You already have a ticket open.`);
  if (message.guild.channels.find(c => c.name == "✉Tickets✉" && c.type == "category")) return message.guild.createChannel(`ticket-[${message.author.username}] ${message.id}`, "text")
.then(c => {
  let category = client.channels.find(c => c.name == "✉Tickets✉" && c.type == "category")
  c.setParent(category.id);
      let role = message.guild.roles.find("name", "Support Team");
      let role2 = message.guild.roles.find("name", "@everyone");
      c.overwritePermissions(role, {
          SEND_MESSAGES: true,
          READ_MESSAGES: true
      });
      c.overwritePermissions(role2, {
          SEND_MESSAGES: false,
          READ_MESSAGES: false
      });
      c.overwritePermissions(message.author, {
          SEND_MESSAGES: true,
          READ_MESSAGES: true
      });
      var cname =  message.guild.channels.find(channel => channel.name === `${c.name}`).toString();
      const embed1 = new Discord.RichEmbed()
      .setColor(0xCF40FA)
      .addField(`${emoji} Ticket was created!`, `${cname}`)
      message.channel.send({embed: embed1})
      const embed = new Discord.RichEmbed()
      .setColor(0xCF40FA)
      .addField(`Hey ${message.author.username}!`, `Please try explain why you opened this ticket with as much detail as possible. Our **Support Team** will be here soon to help.`)
      .addField(`your message was:`, `${reason}`)
      .setFooter(`${message.settings.footer}`)
      .setTimestamp(message.createdAt)
  c.send({
      embed: embed
  });
    //      client.channels.get("name", `ticket-${message.author.tag}`).send({embed: embed1});
  }).catch(console.error);
  };
//} else {
 // message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds + "Or " + ticketcmds).then(msg => msg.delete(5000))}


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};


exports.help = {
  name: "new",
  category: "Miscelaneous",
  description: "Creates a new support ticket",
  usage: "new <REASON>"
};
