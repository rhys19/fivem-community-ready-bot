    
module.exports.run = async(Client, message, args) => {
  const botcmds = message.settings.botcmds

  if (message.channel.id === `${message.settings.botcmds}`) {
    message.delete()
    if (args[0] === "bot-shit" || message.channel.name.includes("bot-shit")) 
    {
        message.channel.send(
        `:warning: __**Support Template**__ :warning:
        
What is your issue?
When does the issue occur?
Have you installed it correctly?
Have you read the correct documentation in order to install any dependencies, if applicable?`
        )
    }
    else 
    {
        message.author.send("Could not find template to use.")
    }
  } else {
    message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds).then(msg => msg.delete(5000))
  }
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 0
};

exports.help = {
  name: 'template',
  description: 'template',
  usage: 'template'
};