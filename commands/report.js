const Discord = require("discord.js")

module.exports.run = async (bot, message, args) => {
    let rUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
        if(!rUser) return message.channel.send("Please mention a user");
        let reason = args.join(" ").slice(22);

        let reportEmbed = new Discord.RichEmbed()
        .setDescription("Reports")
        .setColor("#00fff2")
        .addField("Reported User", `${rUser} with ID: ${rUser.id}`)
        .addField("Reported By", `${message.author} with ID: ${message.author.id}`)
        .addField("Channel", message.channel)
        .addField("Time", message.createdAt)
        .addField("Reason", reason)
.setFooter(`${bot.config.footer}`) 
        let reportschannel = message.guild.channels.find(`name`, "mod-logs");
       if(!reportschannel) return message.channel.send("Couldn't find mod-log channel.");

        message.delete().catch(O_o=>{});
        reportschannel.send(reportEmbed);
}


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};


exports.help = {
  name: "report",
  category: "Miscelaneous",
  description: "report Information",
  usage: "report"
};
