
const Discord = require("discord.js")

module.exports.run = async (bot, message, args) => {
  const botcmds = message.settings.botcmds

 // if (message.channel.id === `${message.settings.botcmds}`) {
    message.delete()
    let Suggestion = args.slice(0).join(" ");

    if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.noPerms(message, "MANAGE_MESSAGES");
    if (args.length === 0)
    return message.reply(`**Invalid Format:** ${message.settings.prefix} <Suggestion>`)

    const embed = new Discord.RichEmbed()
    .setTitle("A Suggestion Has Been Created!")
    .setColor("#5599ff")
    .setDescription(`${Suggestion}`)
.setFooter(`${bot.config.footer}`) 
    .setFooter(`Suggestion Created By: ${message.author.username}`, `${message.author.avatarURL}`)
  
    message.channel.send({embed}).then( (message) => {
        message.react('👍')
        .then(() => message.react('👎'))
    });

  //} else {
    //message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds).then(msg => msg.delete(5000))
  //}
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "suggest",
  category: "Miscelaneous",
  description: "creates a suggestion",
  usage: "suggest <Suggestion>"
};