exports.run = (client, message, args, level) => {
//client.on(`guildMemberAdd`, member =>{

  //let guild = member.guild;
  const guild = client.guild
  //et members = client.members.filter(member => member.bot > bot);
  let embed1 = new Discord.RichEmbed()
  .setColor([255, 0, 0])
  .setThumbnail(`${message.guild.iconURL}`)
  .setAuthor("Server Information:")
  .addField("Server Rules", "Please follow all our rules!")
  .addField(`Welcome to`, `${message.guild.name}`)

.addField(`Here are a few rules we have`)

 .addField(`#1 No Fail RP `, `- Consquences (First Warning) (Second Kick)`)

 .addField(`#2 No VDM/RDM`, `- Consquences (First Warning) (Second Banned) - if heard/saw`)

.addField(`#3 No Disrespect to staff/members`, `- Consquences (First Warning) (Second Kick) - if heard/saw`)

.addField(`#4 No Unrealistic Driving`, `- Consquences (First Warning) (Second Kick) - if heard/saw`)

.addField(`#5 No Unrealistic Vehicles`, `- Consquences (First Warning) (Second Kick) - if heard/saw`)

.addField(`#6 No Speedboosting (2X only)`, `- Consquences (First Warning) (Second Banned) - if heard/saw`)

.addField(`#7 No Godmode`, `- Consquences (First Warning) (Second Banned) - if heard/saw`)

.addField(`#8 No Bullying`, `- Consquences (First Warning) (Second Banned) - if heard/saw`)

.addField(`#9 No Reviving If You Die (Unless NO EMS is on)`, `- Consquences (First Warning) (Second Kick) - if heard/saw`)

.addField(`#10 No Racism`, `- Consquences (First Warning) (Second Banned) - if heard/saw`)

.addField(`#11 No No-Clipping in AOP`, `- Consquences (First Warning) (Second Kick) - if heard/saw`)

.addField(`#12 No Refusing To Listen To Staff`, `- Consquences (First Warning) (Second Kick) - if heard/saw`)

.addField(`#13 No Advertising`, `- Consquences (First Warning) (Second Banned) - if heard/saw`)

.addField(`#14 Do Not Mention Owner's!`, `- consquences (First Warning) (Second Kick) - if heard/saw`)

.addField(`#15 Do Not Argue With staff members!`, `- Consquences (First Warning) (Second Kick) - if heard/saw`)

.addField(`#16 Do Not DM Directors make a ticket if you have an issue! (if in DND or offline)`, `- Consquences (First Warning) (Second Kick) (Third Banned)`)

.addField(`#17 DO NOT THREATEN STAFF`, `- Consquences (FIRST WARNING) (SECOND BAN)`)
.addField(`#18 Use Common Sense!`, `- Consquences (First Warning) (Second Kick) (third ban!)`)

.addField(` -Administrators have final say!`)

.addField(` -Have Fun!`, ``)
.setFooter(`${client.config.footer}`) 
  message.channel.send({embed: embed1})
  message.delete()
};

const Discord = require('discord.js');
const client = new Discord.Client(); // This uses the discord.js package to setup a client


  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: "User"
  };


  exports.help = {
    name: "rules",
    category: "Miscelaneous",
    description: "Server Rules",
    usage: "rules"
  };
