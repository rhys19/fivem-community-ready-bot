const Discord = require('discord.js')


var links = ["https://cad.us-rp.life", "https://forums.us-rp.life/", "https://bas.us-rp.life/", "https://donate.us-rp.life/", "https://discord.gg/qkBn3EX"]

var forums = "https://forums.affinityliferp.com";
var cad = "https://cad.affinityliferp.com";
var bas = "https://bas.affinityliferp.com";
var donate = "https://donate.affinityliferp.com";
var dcInv = "https://discord.gg/qkBn3EX";
var bcad = "http://cad.affinityliferp.com/"
var dpage = "https://donate.affinityliferp.com/"

module.exports.run = async (Client, message, args) => {
  message.delete()
  const botcmds = message.guild.channels.find("id", message.settings.botcmds)
        let embed = new Discord.RichEmbed()
        .setTitle("Links")    
        .setColor('#FA3838')
        //.setDescription(`Cad URL: ${message.settings.cadlink}`)
        .addField("Forums", `${forums}`)
        .addField("BAS", `${bas}`)
        .addField("Donate", `${donate}`)
        .addField("Pern Invite", `${dcInv}`)
        .addField("Regular CAD", `${cad}`)
        .addField("Backup CAD", `${bcad}`)
		.addField("Donate Page", `${dpage}`)
        return message.channel.send(embed)
  };

  exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "user"
};

exports.help = {
  name: "links",
  category: "Miscelaneous",
  description: "Shows server links",
  usage: "links"
};
