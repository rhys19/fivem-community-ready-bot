exports.run = (client, msg, args) => {
  message.delete()
  function typeCheck() {
  let mention = msg.mentions.channels.first();

  if (!mention) {
     let channelName = msg.content.split(" ")[0];
     return msg.guild.channels.find(c => c.name === channelName);
  } else {
     return mention;
  }
}

//defines returnedChannel as the output of the typeCheck() function
let returnedChannel = typeCheck()

let embed1 = new Discord.RichEmbed()
.setTitle("Deleting Channel")
.setDescription(`Deleting Channel: <${returnedChannel}>`)
.setColor([255, 0, 0])
.addField(`Command ran by:`, `${msg.author.username}`)
.setTimestamp(msg.createdAt)
msg.channel.send({embed: embed1})
msg.channel.send("Deleting Channel Please Wait 15 Seconds.")
returnedChannel.send("Channel will be deleted in 15 seconds!!!")
returnedChannel.delete(15000)
};

const Discord = require('discord.js');
//const client = new Discord.Client(); // This uses the discord.js package to setup a client


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "Admin"
};


exports.help = {
  name: "delc",
  category: "System",
  description: "Delete a channel",
  usage: "delc <channelname>"
};
