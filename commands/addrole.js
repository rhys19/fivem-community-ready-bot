const Discord = require("discord.js");

exports.run  = async (client, message, args) => { 
    message.delete()	

if(!message.member.hasPermission("MANAGE_MEMBERS")) return message.reply("Sorry pal, you can't do that!");

var args2 = args.slice(1).join(" ");
let role = message.guild.roles.find(r => r.name === args2);
let member = message.mentions.members.first();
if(member.roles.has(role.name)) return message.reply("They already have that role!");
member.addRole(role).catch(console.error);
message.channel.send(`Congrats to <@${member.id}>, they have been given the role ${role.name}`);
member.send(`Congrats, you have been given the role ${role.name}!`)
message.channel.send(`We tried to DM ${member.id} but their dms were locked or they blocked the bot, so here is the msg: Congratz ${member.id}, you have been given the role ${role.name}`);
//member.removeRole(role).catch(console.error);
};
    
    const client = new Discord.Client(); // This uses the discord.js package to setup a client
    
    
    
    exports.conf = {
      enabled: true,
      guildOnly: false,
      aliases: [],
      permLevel: "Admin"
    };
    
    
    exports.help = {
      name: "addrole",
      category: "System",
      description: "add a role to a user",
      usage: "addrole <role>"
    };
    