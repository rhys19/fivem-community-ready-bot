exports.run = (client, message, args, level) => {
  message.delete()
  if (process.version.slice(1).split(".")[0] < 10) message.channel.send("Node 10.14.1 or higher is required. Update Node on your system.")
else
  message.channel.send(`Current Version of NodeJS: ${process.version}`)
  message.delete()
};

const Discord = require('discord.js');
//const fs = require("fs");
//const db = require("quick.db");
//onst i = require("fs");
const client = new Discord.Client(); // This uses the discord.js package to setup a client


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};


exports.help = {
  name: "nodev",
  category: "Miscelaneous",
  description: "node Information",
  usage: "nodev"
};
