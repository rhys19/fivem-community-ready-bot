exports.run = (client, message, args) => {
  message.delete()
  if (!args || args.length < 1) return message.reply("Must mention a member!");
let embed1 = new Discord.RichEmbed()
.setTitle("Patrol Update")
.setDescription("Patrol Exclusions")
.setColor([255, 0, 0])
.setThumbnail(`${message.guild.iconURL}`)
.setTitle('Member Exclusion List')
.addField('Time:', '5:00 P.M EST')
.addBlankField()
.addField("Server:", "Server 1")
.addBlankField()
.addField('Members Excluded:', `${args[0]}`)
.addBlankField()
.setFooter(`${client.config.footer}`) 
message.channel.send({embed: embed1})
  message.delete()
  if (!message); return
  return message.reply(`Command doesn't Exist please do ${message.settings.prefix} ;)`);
};

const Discord = require('discord.js');
const client = new Discord.Client(); // This uses the discord.js package to setup a client

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "Admin"
};


exports.help = {
  name: "excludeuser",
  category: "System",
  description: "Excludes a member from a required patrol!",
  usage: "excludeuser"
};
