const Discord = require('discord.js')

module.exports.run = async (client, message, args) => {

  rebootmsg1 = new Discord.RichEmbed()
  .setDescription("Restarting bot...")
  .setFooter("This process may take up to 1 minute.")
  .setColor("#F4613F");

rebootmsg2 = new Discord.RichEmbed()
  .setDescription("Bot has restarted!")
  //.setFooter(client.commands.size + " errors encountered")
  .setColor("#417af4");

//    if(!message.member('282237284349116417')) return

    message.channel.send(rebootmsg1)
    .then(client.destroy())
    .then(client.login(client.config.token))
    .then(message.channel.send(rebootmsg2));
}

 // .then(client => console.log(`Ping: ${client.ping}\n  Process restarted!`)));
//};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 9
};

exports.help = {
  name: "restart",
  category: "System",
  description: "Reboots the bot",
  usage: "restart"
};