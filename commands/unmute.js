const Discord = require('discord.js');
exports.run = (client, message, args) => {
  let reason = args.slice(1).join(' ');
  let user = message.mentions.users.first();
  let nickname = message.guild.members.get(user.id).nickname;
  let name = message.guild.members.get(user.id).name
  let modlog = client.channels.find('name', 'mod-logs');
  let muteRole = client.guilds.get(message.guild.id).roles.find('name', 'Muted');
  if (!modlog) return message.reply('I cannot find a mod-log channel').catch(console.error);
  if (!muteRole) return message.reply('I cannot find a mute role').catch(console.error);
  if (message.mentions.users.size < 1) return message.reply('You must mention someone to unmute them.').catch(console.error);
  const embed = new Discord.RichEmbed()
    .setColor(0x00AE86)
    .setTimestamp()
    .addField('Action:', 'Unmute')
    .addField('User:', `${user.username}#${user.discriminator}`)
    .addField('Modrator:', `${message.author.username}#${message.author.discriminator}`)
.setFooter(`${client.config.footer}`) 
  if (!message.guild.member(client.user).hasPermission('MANAGE_ROLES_OR_PERMISSIONS')) return message.reply('I do not have the correct permissions.').catch(console.error);
    message.guild.member(user).removeRole(muteRole).then(() => {
		message.guild.members.get(user.id).setNickname(user.username)
		message.channel.send(user+" Has been unmuted")
      client.channels.get(modlog).sendEmbed(embed).catch(console.error);
	})
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 3
};

exports.help = {
  name: "unmute",
  category: "System",
  description: "unmutes a mentioned user",
  usage: "unmute [mention]"
};