const { version } = require("discord.js");
const moment = require("moment");
require("moment-duration-format");


exports.run = (client, message, args, level) => { // eslint-disable-line no-unused-vars
  const botcmds = message.settings.botcmds

  if (message.channel.id === `${message.settings.botcmds}`) {
  const duration = moment.duration(client.uptime).format(" D [days], H [hrs], m [mins], s [secs]");
  var guilds = client.guilds.array().sort()
  message.channel.send(`= STATISTICS =
• Mem Usage  :: ${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB
• Uptime     :: ${duration}
• Users      :: ${client.users.size.toLocaleString()}
• Servers    :: ${client.guilds.size.toLocaleString()}
• Channels   :: ${client.channels.size.toLocaleString()}
• Discord.js :: v${version}
• Node       :: ${process.version}
• Servers :: ` + guilds, {code: "asciidoc"});
    message.delete();
  } else {
    message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds).then(msg => msg.delete(5000))  }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};


exports.help = {
  name: "stats",
  category: "Miscelaneous",
  description: "Gives some useful bot statistics",
  usage: "stats"
};
