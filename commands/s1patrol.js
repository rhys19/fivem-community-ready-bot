exports.run = (client, message, args) => {
let time = args.slice(0).join(" ");
//let args = args.slice(0).join(" ");
const ip1 = message.settings.serverip
const s1name = message.settings.servername
let embed1 = new Discord.RichEmbed()
.setTitle("Patrol announcement")
.setDescription(`Patrol Starting at: ${time}`)
.setColor([255, 0, 0])
.setThumbnail(`${message.guild.iconURL}`)
.setTitle('Patrol Announcement')
.addBlankField()
.addField("Server:", "1")
.addBlankField()
.addField(`Server Name:`, `${s1name}`)
.addBlankField()
.addField("Server IP:", `${ip1}`)
.addBlankField()
.addBlankField()
.addField("NOTE:", `__if you cannot make it please create a ticket using ${message.settings.prefix}new (reason) and a staff member will respond asap!__`)
.addBlankField()
.setFooter(`${client.config.footer}`)
	if (!args.length) {
		return message.channel.send(`Invalid Command Arguments, ${message.author}!`);
	}
  message.channel.send({embed: embed1})
  message.channel.send("@everyone")
  message.delete()
};

const Discord = require('discord.js');
const client = new Discord.Client(); // This uses the discord.js package to setup a client



exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "Admin"
};


exports.help = {
  name: "s1patrol",
  category: "System",
  description: "Announce server 1 patrol!",
  usage: "s1patrol"
};
