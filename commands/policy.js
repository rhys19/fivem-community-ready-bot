const request = require('request')
const Discord = require('discord.js')


function error(channel, ip, errorCode, errCode) {
    errCode = "Could not determine status code."
    if (errorCode.toString() === "1") 
        errCode = "404 - `info.json could not be reached.`"
    if (errorCode.toString() === "2")
        errCode = "404 - `players.json could not be reached.`"
    if (errorCode.toString() === "3")
        errCode = "404 - `Server policy could not be obtained.`"
   let embed = new Discord.RichEmbed()
    .setTitle("Error")    
    .setColor('#FA3838')
    .setDescription("Server could not be reached.")
    .addField("Error Code", errCode)
    .setFooter(ip)
    channel.send(embed)
}

module.exports.run = async (Client, message, args) => {
    message.delete()
    let IP = message.settings.serverip;
    let server = message.settings.serverip;

    if(!server) {
        let embed = new Discord.RichEmbed()
        .setTitle("Error")    
        .setColor('#FA3838')
        .setDescription("Server could not be found.")
        return message.channel.send(embed)
    }
    else {
        IP = message.settings.serverip
    }

    try {
        let api1 = `http://${IP}/players.json`
        let api2 = `http://${IP}/info.json`
        request.get(api2, {timeout: 2000},function (err, response, main) {
            if (err) return error(message.channel, `IP: ${IP}`, 1)
            request.get(api1, {timeout: 2000},function (err, response, body) {
                if (err) return error(message.channel, `IP: ${IP}`, 2)
                request.get(`https://policy-live.fivem.net/api/policy/${JSON.parse(main).vars.sv_licenseKeyToken}`, {timeout: 2000}, function(err, response, content) {
                    if (err) return error(message.channel, `IP: ${IP}`, 3)
                    try {
                        var start = JSON.parse(body)
                        var start2 = JSON.parse(main)
                        var start3 = JSON.parse(content)
                
                        if (start == null || start == []) {
                            var playersCount = 0
                        } else {
                            var playersCount = start.length;
                        }

                        if (start3 === [] || start3.length === 0) {
                            var policy = `None`
                        } else {
                            var policy = `${start3.join("\n")}`
                        }
                        
                        var embed = new Discord.RichEmbed()
                        .setColor("#54C86D")
                        .setAuthor("Patreon Subscription Info" , 'https://i.imgur.com/PnzJ35e.png')
                        .addField("Server IP", IP)
                        .addField("Server License Key Token", start2.vars.sv_licenseKeyToken)
                        .addField("Patreon Subscription", policy)

                        message.channel.send(embed);
                    } catch (err) {
                        error(message.channel, `Server IP: ${Client.config.serverip}`)
                        console.log(err.toString())
                    }

                    })

                })
        
            })
        
    } catch (err) {
        return;
    }
  };


  exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "user"
};

exports.help = {
  name: "policy",
  category: "Moderation",
  description: "Shows Patreon Subscription info",
  usage: "policy"
};
