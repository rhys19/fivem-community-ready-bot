exports.run = (client, message, args) => {
message.delete()
let mip = message.settings.mip
let embed1 = new Discord.RichEmbed()
.setTitle("Server IP")
.setDescription(`${mip}`)
.addField("Credits", "Rhys19")
.setColor([255, 0, 0])
.setTimestamp(message.createdAt)
.setFooter(`Made:`)
message.channel.send({embed: embed1})
};

const Discord = require('discord.js');
const client = new Discord.Client(); // This uses the discord.js package to setup a client

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "Member"
};


exports.help = {
  name: "mip",
  category: "System",
  description: "Get Server IP",
  usage: "mip"
};
