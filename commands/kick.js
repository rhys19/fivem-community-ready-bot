const Discord = require("discord.js")

module.exports.run = async (client, message, args) => {
    message.delete()
    let kUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
        if(!kUser) return message.channel.send(" Haha, what a name, maybe try a real one?");
        let kReason = args.join(" ").slice(22);
		if(!kReason) return message.channel.send("Haha what a reason, how about a real reason?");
        if(!message.member.hasPermission("KICK_MEMBERS")) return message.channel.send("Your joking right? Have fun NOT kicking");
        if(kUser.hasPermission("KICK_MEMBERS")) return message.channel.send("Hmmmm. Should I still be trusting you?");
        let kickEmbed = new Discord.RichEmbed()
        .setDescription("Kick")
        .setColor("#fc4b4b")
        .addField("Kicked User", `${kUser} with ID ${kUser.id}`)
        .addField("Kicked By", `<@${message.author.id}> with ID ${message.author.id}`)
        .addField("Kicked In", message.channel)
        .addField("Time", message.createdAt)
        .addField("Reason", `${kReason}.`)
		.setFooter(`${client.config.footer}`) 
        let modlogchan = `${client.settings.modlogchannel}`;
        let kickChannel = message.guild.channels.find(`name`, `${modlogchan}`);
         if (!kickChannel) return message.reply(`I cannot find channel: ${modlogchan}`);
        message.guild.member(kUser).kick(kReason);
        kickChannel.send(kickEmbed);
        message.channel.send(`:Success: ${kUser} **has been kicked from the Discord.**`).then(msg => msg.delete(6000));
    message.delete();
}

exports.conf = {
  aliases: [],
  permLevel: 2
};

module.exports.help = {
      name: 'kick',
  category: "System",
  description: 'Kicks the mentioned user.',
  usage: 'kick [mention] [reason]'
}