exports.run = (client, message, args, level) => {
  message.delete()
  if (message.channel.id === '629107222802202634') {
let embed1 = new Discord.RichEmbed()
.setColor([255, 0, 0])
.setThumbnail(`${message.guild.iconURL}`)
.setTitle("Using codeblocks!")
.setDescription("This shows you how to use code blocks! example below look at the footer for the key your supposed to use!")
.addField("Example:", "```Test code here```")
.setFooter("use the tilda key (`) 3 at the beginning and 3 at the end! like so: ```Test code here```")
.setTimestamp()
//.setFooter(`${client.config.footer}`) 
message.channel.send({embed: embed1})
  message.delete()
} else {
  message.reply("Command: `" + `${message}` + "` must be sent in <#629107222802202634>").then(msg => msg.delete(5000))
}
};

const Discord = require('discord.js');
//const fs = require("fs");
//const db = require("quick.db");
//onst i = require("fs");
const client = new Discord.Client(); // This uses the discord.js package to setup a client


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};


exports.help = {
  name: "codeblock",
  category: "Miscelaneous",
  description: "Code Block",
  usage: "codeblock"
};
