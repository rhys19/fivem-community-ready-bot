exports.run = (client, message, args, level) => {
  const botcmds = message.settings.botcmds

  if (message.channel.id === '629107222802202634') {
//client.on(`guildMemberAdd`, member =>{

  //let guild = member.guild;
  const guild = client.guild
  //et members = client.members.filter(member => member.bot > bot);
  let embed1 = new Discord.RichEmbed()
  .setColor([255, 0, 0])
  //.setThumbnail(`${message.guild.iconURL}`)
  //.setAuthor("Server Information:")
  .addField("Server Information:", "Information about the server!")
  .addField(`Members:`, `${message.guild.members.filter(member => !member.user.bot).size}`)
  //.addBlankField()
  .addField(`Total Users:`, `${message.guild.members.filter(member => !member.user.bots).size}`)
  .addField(`Bots:`, `${message.guild.members.filter(m => m.user.bot).size}`)
  .addField(`Server Owner:`, `${message.guild.owner}`)
  //.addBlankField()
  .addField(`Roles:`, `${message.guild.roles.size}`)
  //.addBlankField()
  .addField(`Channels:`, `${message.guild.channels.size}`)
  .addField(`Server Name:`, `${message.guild.name}`)
  //.addBlankField()
  .addField(`Date Created:`, `${message.guild.createdAt}`)
.setFooter(`${client.config.footer}`) 
  message.channel.send({embed: embed1})
  message.delete()
} else {
  message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds).then(msg => msg.delete(5000))}
};

const Discord = require('discord.js');
const client = new Discord.Client(); // This uses the discord.js package to setup a client

  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: "User"
  };


  exports.help = {
    name: "serverinfo",
    category: "Miscelaneous",
    description: "Gives some useful bot statistics",
    usage: "serverinfo"
  };
