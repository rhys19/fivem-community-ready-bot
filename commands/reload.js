exports.run = async (client, message, args, level) => {// eslint-disable-line no-unused-vars
  if (!args || args.length < 1) return message.reply("Must provide a command to reload.");
  //message.delete()
//message.delete()
  let response = await client.unloadCommand(args[0]);
  //message.delete()
  if (response) return message.reply(`Error Unloading: ${response}`);
//message.delete()
  response = client.loadCommand(args[0]);
  if (response) return message.reply(`Error Loading: ${response}`);
//message.delete()
  message.reply(`The command \`${args[0]}\` has been reloaded`);
  //message.delete()
    if (!message); return
  return message.reply("Command doesn't Exist please do -help ;)");
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "Bot Admin"
};

exports.help = {
  name: "reload",
  category: "System",
  description: "Reloads a command that\"s been modified.",
  usage: "reload [command]"
};
