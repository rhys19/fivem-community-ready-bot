exports.run = (client, message, args, level) => {
  const botcmds = message.settings.botcmds
  message.delete()
  if (message.channel.id === '629107222802202634') {
let embed1 = new Discord.RichEmbed()
.setColor([255, 0, 0])
.setThumbnail(`${message.guild.iconURL}`)
.addField("https://en.wikipedia.org/wiki/Nuclear_power_plant", "Info about Nuclear Power Plants ;)")
.setFooter(`${client.config.footer}`) 
message.channel.send({embed: embed1})
  message.delete()
} else {
  message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds).then(msg => msg.delete(5000))}
};

const Discord = require('discord.js');
const client = new Discord.Client(); // This uses the discord.js package to setup a client

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};


exports.help = {
  name: "npp",
  category: "Miscelaneous",
  description: "Get info about how nuclear power plants work ;)",
  usage: "npp"
};
