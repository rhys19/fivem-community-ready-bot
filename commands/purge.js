const Discord = require("discord.js")

exports.run = (client, message, args) => {
  message.delete()
  const messagecount = parseInt(args.join(' '));
  message.channel.fetchMessages({
    limit: messagecount
  }).then(messages => message.channel.bulkDelete(messages));
if (args[0] == " ") return
  (messages => message.channel.bulkDelete(messages));
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 2
};

exports.help = {
  name: "purge",
  category: "System",
  description: "Purges X amount of messages from a given channel.",
  usage: "purge <number>"
};