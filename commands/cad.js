const Discord = require('discord.js')

module.exports.run = async (Client, message, args) => {
  const botcmds = message.guild.channels.find("id", message.settings.botcmds)
        let embed = new Discord.RichEmbed()
        .setTitle("CAD/MDT")    
        .setColor('#FA3838')
        .setDescription(`Cad URL: ${message.settings.cadlink}`)
        return message.channel.send(embed)
  };

  exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "user"
};

exports.help = {
  name: "cad",
  category: "Miscelaneous",
  description: "Shows server cad",
  usage: "cad"
};
