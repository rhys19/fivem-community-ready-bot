const Discord = require("discord.js");

exports.run  = async (client, message, args) => { 
    message.delete()	

if(!message.member.hasPermission("MANAGE_MEMBERS")) return message.reply("Sorry pal, you can't do that!");

var args2 = args.slice(1).join(" ");
var args3 = args.slice(3).join(" ");
let reason = args.join(" ").slice(2);

let role = message.guild.roles.find(r => r.name === args2);
let member = message.mentions.members.first();
//if(!member.roles.has(role.name)) return message.reply("They Don't have that role!");
member.removeRole(role).catch(console.error);
message.channel.send(`Sorry <@${member.id}>, You have been demoted/removed from ${role.name}!`);
member.send(`Sorry, you have been Demoted/removed from the role ${role.name}!`);
};
    
    const client = new Discord.Client(); // This uses the discord.js package to setup a client
    
    
    
    exports.conf = {
      enabled: true,
      guildOnly: false,
      aliases: [],
      permLevel: "Admin"
    };
    
    
    exports.help = {
      name: "removerole",
      category: "System",
      description: "removes a role to a user",
      usage: "removerole <role>"
    };
    