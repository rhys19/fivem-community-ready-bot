const Discord = require('discord.js');
exports.run = (client, message, args) => {
  message.delete()
	
  let reason = args.slice(1).join(' ');
  let user = message.mentions.users.first();
  let nickname = message.guild.members.get(user.id).nickname;
  let name = message.guild.members.get(user.id).name
  let modlog = client.channels.find(c => c.name === 'mod-logs');
  let muteRole = client.guilds.get(message.guild.id).roles.find(r => r.name === 'Muted');
  if (!modlog) return message.reply('I cannot find a mod-log channel').catch(console.error);
  if (!muteRole) return message.reply('I cannot find a mute role').catch(console.error);
  if (message.mentions.users.size < 1) return message.reply('You must mention someone to mute them.').catch(console.error);
  if (reason.length < 1) return message.reply('You must supply a reason for the mute.').catch(console.error);
  const embed = new Discord.RichEmbed()
    .setColor(0x00AE86)
    .setTimestamp()
    .addField('Action:', 'Un/Mute')
    .addField('User:', `${user.username}#${user.discriminator}`)
    .addField('Modrator:', `${message.author.username}#${message.author.discriminator}`)
.setFooter(`${client.config.footer}`) 
  if (!message.guild.member(client.user).hasPermission('MANAGE_ROLES_OR_PERMISSIONS')) return message.reply('I do not have the correct permissions.').catch(console.error);
    message.guild.member(user).addRole(muteRole).then(() => {
		message.channel.send(user+" Has been muted for "+reason)
		message.guild.members.get(user.id).setNickname("[MUTED]"+user.username)
      client.channels.get(modlog).sendEmbed(embed).catch(console.error);
    });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 3
};

exports.help = {
  name: "mute",
  category: "System",
  description: "mutes or unmutes a mentioned user",
  usage: "un/mute [mention]"
};