exports.run = (client, message, args, level) => {
  const botcmds = message.settings.botcmds

  if (message.channel.id === `${message.settings.botcmds}`) {

let embed1 = new Discord.RichEmbed()
.setColor([255, 0, 0])
.setThumbnail(`${message.guild.iconURL}`)
.setTitle(`Important URL's for ${message.settings.dcAbbrev}`)
.setDescription(`${message.guild.name} URL's`)
.addField(`CAD/MDT Link`, `${message.settings.cadlink}`)
.addField(`Forums Site`, `${message.settings.forumsURL}`)
.addField(`Perm Discord Invite Link`, `${message.settings.DcURL}`)
.addField(`Server IP`, `${message.settings.serverip}`)
.setFooter(`${client.config.footer}`) 
message.channel.send({embed: embed1})
  message.delete()
} else {
  message.reply("Command: `" + `${message}` + "` must be sent in " + botcmds).then(msg => msg.delete(5000))}

};

const Discord = require('discord.js');
//const fs = require("fs");
//const db = require("quick.db");
//onst i = require("fs");
const client = new Discord.Client(); // This uses the discord.js package to setup a client



exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};


exports.help = {
  name: "sites",
  category: "Miscelaneous",
  description: "Get all of our websites!",
  usage: "sites"
};
