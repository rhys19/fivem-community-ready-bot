// This event executes when a new member joins a server. Let's welcome them!
const Discord = require('discord.js');
const client = new Discord.Client(); // This uses the discord.js package to setup a client
module.exports = (client, member, message) => {
  // Load the guild's settings
  const settings = client.getGuildSettings(member.guild);

  // If welcome is off, don't proceed (don't welcome the user)
  if (settings.welcomeEnabled !== "true") return;

  // Replace the placeholders in the welcome message with actual data
  const welcomeMessage = settings.welcomeMessage.replace("{{user}}", member.user.tag);

  // Send the welcome message to the welcome channel
  // There's a place for more configs here.
let embed1 = new Discord.RichEmbed()
.setColor([255, 0, 0])
.setDescription(`${welcomeMessage}`)
.setFooter(`${client.config.footer}`) 
message.channel.send({embed: embed1}).catch(console.error);
};