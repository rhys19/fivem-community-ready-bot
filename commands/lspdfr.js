exports.run = (client, message, args, level) => {
  const botcmds = message.settings.botcmds
  message.delete()
let embed1 = new Discord.RichEmbed()
.setColor([255, 0, 0])
.setThumbnail(`${message.guild.iconURL}`)
.setTitle("LSPDFR Keybinds")
.setDescription(`Keybinds for LSPDFR`)
.addField("Interaction Menu", `Modifier + U`)
.addField("Mimic/Follow", `Modifier + Y`)
.addField("Traffic Stop Menu", `Modifier + E`)
.addField("Pullover Keybinding", `Modifier + Shift`)
.addField("Keybinding Modifier", `CTRL`)
.setFooter(`${client.config.footer}`) 
message.channel.send({embed: embed1})
let embed2 = new Discord.RichEmbed()
.setColor([255, 0, 0])
.setThumbnail(`${message.guild.iconURL}`)
.setTitle("LSPDFR Commands")
.setDescription(`Keybinds for LSPDFR`)
.addField("Callouts Menu", `/callouts`)
.addField("follow", `/follow`)
.addField("Mimic", `/mimic`)
.addField("handcuff", `/handcuff`)
.addField("callout", `/callout <CALLOUTNAME>`)
.setFooter(`${client.config.footer}`) 
message.channel.send({embed: embed2})
  message.delete()
};

const Discord = require('discord.js');
const client = new Discord.Client();

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "BotOwner"
};


exports.help = {
  name: "lspdfr",
  category: "Miscelaneous",
  description: "LSPDFR Information",
  usage: "lspdfr"
};
