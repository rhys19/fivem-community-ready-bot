# FiveM Community Ready Bot

Ever wanted a bot to do all your announcements, warns, kicks, or bans? even play music? well you've come to the right place!

## Installation

Step 1: Goto https://discordapp.com/developers/applications/ and create an app + bot
Step 2: Copy the token and paste it inside `token: ""`, which is located inside `config.js`

## Usage

TODO: Write all the commands

- addrole (rolename)
- announce (message)
- aop (new aop)
- apic (resourcename)
- away (set bot to away)
- ban (user)
- cad (show cad url)
- changelog (new items)
- clone (Doesn't work)
- codeblock (shows how to use codeblocks)
- connect (shows ips)
- create (unkown)
- credits (Shows bot credits)
- db (N/A)
- delc (N/A)
- delete (N/A)
- dnd (set bot to dnd)
- draw (N/A)
- embed (Create an embed)
- eval (N/A) DANGEROUS
- google (google something)
- help (Show help page)
- idgen (creates random number)
- invite (Show bot invite)
- kick (@user)
- links (Show links)
- lockdown (#channel, time)
- lspdfr (Show LSPDFR CMDS)
- mute (@user)
- mylevel.js (idk)
- new (create a ticket)
- nodev (Show node version)
- offline (set bot to offline)
- online (Set bot to online)
- ping (show bot ping)
- players (Show playercount) NOT WORKING
- policy (show element club policy)
- poll (pollname)
- purge (Purge chat)
- reload (Reload a cmd)
- removerole (@role)
- report (@user reason)
- restart (restart bot)
- rules (Show rules)
- s1patrol (Time, Description)
- s2patrol (Time, description)
- serverinfo (show info on guild)
- servers (Show servers)
- set (edit config)
- sites (show sites)
- stats (stats of bot)
- status (status of server)
- suggest (Suggestion)
- template (Show tech template)
- unban (@user)
- unmute (@user)
- uptime (shows bot uptime)
- userinfo (@user)
- warn (@user reason)

## Contributing

1. Fork it
2. Edit the files
3. Make a pull request!
4. I review it and approve it!

## History

TODO: Write history

## Credits

- Discord | Making a great gaming platform!
- Rhys19 | Main owner.
- 

## License

TODO: Get a license